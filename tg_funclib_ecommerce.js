tgNamespace.tgContainer.registerInnerFuncLib("ga", {


    "trackSaleProduct": function (sOrderID, sProductID, oCategory, sProductName, iProductQuantity, sProductCost, oProductbundles, oPar)
    {
        if (typeof oCategory == "string")
        {
            var sCategory = oCategory
        }
        else
        {
            for (sKey in oCategory)
            {
                var sCategory = oCategory[sKey];
                break;
            }
        }
        this.trackSplitter(["_addItem", sOrderID, sProductID, sProductName, sCategory, sProductCost, iProductQuantity]);
    },
    "trackSale": function (sOrderID, sTotal, sTax, sShipping, sCity, sState, sCountry, oPar)
    {
        var sAffiliation = "";
        if (sAffiliation.length > 0)
        {
            sAffiliation = sAffiliation.substr(0, sAffiliation.length - 3);
        }
        this.trackSplitter(["_addTrans", sOrderID, sAffiliation, sTotal, sTax, sShipping, sCity, sState, sCountry]);
        this.isTrackTrans = true;
    },


    "trackAddToCart": function (sProductId, sProductGroup, sProductName, iProductQuantity, sProductCost)
    {
        this.trackSplitter(["_trackEvent", "addToCart", sProductGroup, sProductName, sProductCost * 100]);
    },
    "trackWatchList": function (sProductId, sProductGroup, sProductName, sProductCost)
    {
        this.trackSplitter(["_trackEvent", "addToWatchlist", sProductGroup, sProductName, sProductCost]);
    },
    "trackDeleteFromCart": function (sProductId, sProductGroup, sProductName, sProductQuantity, sProductCost)
    {
        this.trackSplitter(["_trackEvent", "deleteFromCart_Groups", sProductGroup, sProductQuantity, sProductCost]);
        this.trackSplitter(["_trackEvent", "deleteFromCart_Products", sProductName, sProductQuantity, sProductCost]);
    },
    "trackProductView": function (sProductID, sProductGroup, sProductName, sProduktpreis)
    {
        this.trackSplitter(["_trackEvent", "productView", sProductGroup, sProductName, sProduktpreis, true]);
        this.trackSplitter(["_setCustomVar", 4, "productView", sProductID, 3]);
    }
});
