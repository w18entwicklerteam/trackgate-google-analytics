﻿tgNamespace.tgContainer.registerInnerConfig("ga",{
		countFuncLibs: 4,
		countConfigs: 1,
		id:["UA-25428708-3"],
		anonymizeIp:true,
		sVarMergeCookieName : "wrapperGaVarMerging",
	    bUseDoubleClick : true,
        aCluster:   [
                        {"min":0,"max":0},
                        {"min":1,"max":10},
                        {"min":11,"max":50},
                        {"min":51,"max":200},
                        {"min":201,"max":500},
                        {"min":501,"max":1000}
                    ],
        iErrorVarIndex:6,
		sDomain:".oeamtc.at",
		aGoals:		{
					},
		
		sPageGroupIndex: "1",
		aPageGroups2Url:{},
		aUserVarSlots:
					{
						"Usertyp":1,
						"Geschlecht":2,
						"Alter":2
					},
		aPageVarSlots:
					{
						"Seitenart":3,
						"Artikel-Position":4,
						"Artikel-Länge":5
					},
		aSessionVarSlots:
					{
						
					},
		autoLink_catName : "autoLinks",
		autoLink_download_actName : "download",
		autoLink_link_actName : "outgoing_link",
		autoLink_mail_actName : "email",
		campaignTrackingChannelMap :
		{
		    "utm_medium" : "medium_source",
		    "utm_source" : "medium_source",
		    "utm_campaign" : "campaign"
		},
		sUrlPrefix: "",
		uaconfig:{
			
			}
});
