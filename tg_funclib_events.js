tgNamespace.tgContainer.registerInnerFuncLib("ga", {

    "trackEvent": function (oArg)
    {
        this.trackSplitter(["_trackEvent", oArg.group, oArg.name, oArg.value, oArg.intvalue, oArg.noninteraction]);
    },


    "trackTeaserClick": function (oArg)
    {
        this.trackSplitter(["_trackEvent", oArg.site, oArg.position, oArg.element]);
    },

    "trackGoal": function (oArg)
    {
        sGoalId = oArg.id;

        var aCommands = [];
        for (var i = 0; i < this.oLocalConfData.id.length; i++)
        {
            var sGoal = this.readConfig("aGoals", this.oLocalConfData.id[i])[sGoalId];
            if (sGoal === null)
            {
                sGoal = sGoalId;
            }
            aCommands.push(["_trackPageview", "goals/" + sGoal]);
        }
        this.trackSplitter(aCommands);
    },


    "trackDownload": function (oArg)
    {
        var sLinkVal = oArg.link

        var aCommands = [];
        for (var i = 0; i < this.oLocalConfData.id.length; i++)
        {
            var sCatName = this.readConfig("autoLink_catName", this.oLocalConfData.id[i]);
            var sActName = this.readConfig("autoLink_download_actName", this.oLocalConfData.id[i]);
            aCommands.push(["_trackEvent", sCatName, sActName, sLinkVal]);
        }
        this.trackSplitter(aCommands);
    },
    "trackOutLink": function (oArg)
    {
        var sUrl = oArg.url

        var aCommands = [];
        for (var i = 0; i < this.oLocalConfData.id.length; i++)
        {
            var sCatName = this.readConfig("autoLink_catName", this.oLocalConfData.id[i]);
            var sActName = this.readConfig("autoLink_link_actName", this.oLocalConfData.id[i]);
            aCommands.push(["_trackEvent", sCatName, sActName, sUrl]);
        }
        this.trackSplitter(aCommands);
    },
    "trackMailLink": function (oArg)
    {
        var sMail = oArg.mail

        var aCommands = [];
        for (var i = 0; i < this.oLocalConfData.id.length; i++)
        {
            var sCatName = this.readConfig("autoLink_catName", this.oLocalConfData.id[i]);
            var sActName = this.readConfig("autoLink_mail_actName", this.oLocalConfData.id[i]);
            aCommands.push(["_trackEvent", sCatName, sActName, sMail]);
        }
        this.trackSplitter(aCommands);
    },
    "trackSocial": function (oArg)
    {
        this.trackSplitter(["_trackSocial", oArg.target, oArg.action, oArg.url]);
    }
});
