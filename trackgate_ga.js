tgNamespace.tgContainer.registerInnerGate("ga", function (oWrapper, oLocalConfData)
{
    this.oWrapper = oWrapper;
    this.oLocalConfData = oLocalConfData;
    console.log(this.oLocalConfData);
    this._construct = function ()
    {
        this.initial();
        this.oWrapper.registerInnerGateFunctions("ga", "pageview", this.trackPageView);
        this.oWrapper.registerInnerGateFunctions("ga", "event", this.trackEvent);
        this.oWrapper.registerInnerGateFunctions("ga", "error", this.trackError);
        this.oWrapper.registerInnerGateFunctions("ga", "addtocart", this.trackAddToCart);
        this.oWrapper.registerInnerGateFunctions("ga", "sale", this.trackSale);
        this.oWrapper.registerInnerGateFunctions("ga", "!!trackSaleProduct", this.trackSaleProduct);
        this.oWrapper.registerInnerGateFunctions("ga", "productview", this.trackProductView);
        this.oWrapper.registerInnerGateFunctions("ga", "search", this.trackSearch);
        this.oWrapper.registerInnerGateFunctions("ga", "teaser", this.trackTeaserClick);
        this.oWrapper.registerInnerGateFunctions("ga", "uservar", this.trackUserVar);
        this.oWrapper.registerInnerGateFunctions("ga", "sessionvar", this.trackSessionVar);
        this.oWrapper.registerInnerGateFunctions("ga", "pagevar", this.trackPageVar);
        this.oWrapper.registerInnerGateFunctions("ga", "goal", this.trackGoal);
        this.oWrapper.registerInnerGateFunctions("ga", "watchlist", this.trackWatchList);
        this.oWrapper.registerInnerGateFunctions("ga", "deletefromcart", this.trackDeleteFromCart);
        this.oWrapper.registerInnerGateFunctions("ga", "download", this.trackDownload);
        this.oWrapper.registerInnerGateFunctions("ga", "outlink", this.trackOutLink);
        this.oWrapper.registerInnerGateFunctions("ga", "maillink", this.trackMailLink);
        this.oWrapper.registerInnerGateFunctions("ga", "social", this.trackSocial);
        this.oWrapper.registerInnerGateFunctions("ga", "submit", this.submit);
        this.isTrackTrans = false;
    };
    this.initial = function ()
    {
        this.sendTrackPageView = false;
        if (typeof this.oLocalConfData.id == "string")
        {
            this.oLocalConfData.id = [this.oLocalConfData.id];
        }
        window._gaq = window._gaq || [];
        var aCommands = [];
        for (var i = 0; i < this.oLocalConfData.id.length; i++)
        {
            aCommands.push(["_setAccount", this.oLocalConfData.id[i]]);
        }
        this.trackSplitter(aCommands);
        if (this.oLocalConfData["anonymizeIp"] === true)
        {
            window._gaq.push(["_gat._anonymizeIp"]);
        }
        this.trackSplitter(["_setDomainName", this.oLocalConfData["sDomain"]]);
        this.trackSplitter(["_setAllowLinker", true]);
        if (this.oLocalConfData["bUseDoubleClick"] === false || typeof this.oLocalConfData["bUseDoubleClick"] === "undefined")
        {
            (function ()
            {
                var ga = document.createElement("script");
                ga.type = "text/javascript";
                ga.async = true;
                ga.src = ("https:" == document.location.protocol ? "https://ssl" : "http://www") + ".google-analytics.com/ga.js";
                var s = document.getElementsByTagName("script")[0];
                s.parentNode.insertBefore(ga, s);
            })();
        }
        else
        {
            (function ()
            {
                var ga = document.createElement("script");
                ga.type = "text/javascript";
                ga.async = true;
                ga.src = ("https:" == document.location.protocol ? "https://" : "http://") + "stats.g.doubleclick.net/dc.js";
                var s = document.getElementsByTagName("script")[0];
                s.parentNode.insertBefore(ga, s);
            })();
        }
        this.sPageTitle = window.location.pathname;
        this.initVarMerging();
    };
    this.readConfig = function (sConfig, sUA)
    {
        if (typeof sUA === "undefined")
        {
            return this.oLocalConfData[sConfig];
        }
        else
        {
            if (typeof this.oLocalConfData.uaconfig[sUA] !== "undefined" && typeof this.oLocalConfData.uaconfig[sUA][sConfig] !== "undefined")
            {
                return this.oLocalConfData.uaconfig[sUA][sConfig];
            }
            else
            {
                return this.oLocalConfData[sConfig];
            }
        }
    };
    this.trackSplitter = function (aAllCommand)
    {
        if (typeof aAllCommand[0] == "object")
        {
            var iComCount = aAllCommand.length;
            if (iComCount != this.oLocalConfData.id.length)
            {
                this.oWrapper.errorHandler("Inner Wrapper (GA) CONFIG Error (trackSplitter)", {});
            }
            if (iComCount == 1)
            {
                aAllCommand = aAllCommand[0];
            }
        }
        else
        {
            if (typeof aAllCommand[0] == "string")
            {
                var iComCount = 1
            }
        }
        if (this.oLocalConfData.id.length > 1)
        {
            for (var i = 0; i < this.oLocalConfData.id.length; i++)
            {
                if (iComCount == 1)
                {
                    aCommand = aAllCommand;
                }
                else
                {
                    aCommand = aAllCommand[i];
                }
                aCommand[0] = i + "." + aCommand[0];
                window._gaq.push(aCommand);
            }
        }
        else
        {
            window._gaq.push(aAllCommand);
        }
    };
    this.submit = function (oArg)
    {
        if (this.sendTrackPageView !== null)
        {
            var aCommands = [];
            for (var i = 0; i < this.oLocalConfData.id.length; i++)
            {
                aCommands.push(["_trackPageview", this.sendTrackPageView[this.oLocalConfData.id[i]]]);
            }
            this.trackSplitter(aCommands);
        }
        if (this.isTrackTrans === true)
        {
            this.trackSplitter(["_trackTrans"]);
        }
        this.saveVarMerging();
    };
    this.initVarMerging = function ()
    {
        this.oVarValues = {};
        for (var i = 0; i < this.oLocalConfData.id.length; i++)
        {
            var sCookieVal = this.oWrapper.readCookie(this.readConfig("sVarMergeCookieName", this.oLocalConfData.id[i]) + "_" + i);
            if (sCookieVal !== null)
            {
                this.oVarValues[this.oLocalConfData.id[i]] = JSON.parse(decodeURIComponent(sCookieVal));
            }
            else
            {
                this.oVarValues[this.oLocalConfData.id[i]] = {pagevars: {}, sessionvars: {}, uservars: {}};
            }
        }
    };


    //this.index(); => _construct wird jetzt von außen aufgerufen.
});