tgNamespace.tgContainer.registerInnerFuncLib("ga", {

    "trackCampaign": function (oArg)
    {
        oChannels = oArg.channels;
        sParameter = oArg.queryparam;
    },
    "trackPageView": function (oArg)
    {
        sPageTitle = oArg.pagetitle;
        aPageGroup = oArg.pagegroup;

        var aPageGroupCommands = [];
        this.sendTrackPageView = {};
        for (var i = 0; i < this.oLocalConfData.id.length; i++)
        {
            var sPageGroups2Url = "";
            var sPageGroups = "";
            var aPageGroups2Url = this.readConfig("aPageGroups2Url", this.oLocalConfData.id[i]);
            if (typeof aPageGroup == "object")
            {
                for (var sKey in aPageGroup)
                {
                    sPageGroups += aPageGroup[sKey] + "/";
                    if (typeof aPageGroups2Url[sKey] !== "undefined" && aPageGroups2Url[sKey] === true)
                    {
                        sPageGroups2Url += aPageGroup[sKey] + "/";
                    }
                }
                var sPageGroupIndex = this.readConfig("sPageGroupIndex", this.oLocalConfData.id[i]);
                aPageGroupCommands.push(["_setPageGroup", sPageGroupIndex, sPageGroups]);
            }
            var sUrlPrefix = this.readConfig("sUrlPrefix", this.oLocalConfData.id[i]);
            if (typeof sPageTitle === "undefined" || sPageTitle.length === 0 || sPageTitle === false || sPageTitle === null)
            {
                sPageTitle = window.location.pathname;
            }
            if (sPageTitle.substr(0, 1) === "/")
            {
                sPageTitle = sPageTitle.substr(1);
            }
            sTrackUrl = sUrlPrefix + sPageGroups2Url + sPageTitle;
            this.sendTrackPageView[this.oLocalConfData.id[i]] = sTrackUrl;
        }
        this.trackSplitter(aPageGroupCommands);
    },

    "trackError": function (oArg)
    {
        var iVarIndex = this.oLocalConfData["iErrorVarIndex"];
        this.trackSplitter(["_setCustomVar", iVarIndex, "Error", oArg.message, 3]);
    },
    "trackSearch": function ()
    {
        sSearchGroup = oArg.group;
        sSearchString = oArg.term;
        iSearchResult = oArg.countresults;


        var aCommands = [];
        for (var i = 0; i < this.oLocalConfData.id.length; i++)
        {
            var aCluster = this.readConfig("aCluster", this.oLocalConfData.id[i]);
            sCluster = false;
            for (var i = 0; i < aCluster.length; i++)
            {
                if (aCluster[i]["min"] <= iSearchResult && aCluster[i]["max"] >= iSearchResult)
                {
                    sCluster = aCluster[i]["min"] + "-" + aCluster[i]["max"];
                    break;
                }
            }
            if (sCluster == false)
            {
                sCluster = aCluster[aCluster.length - 1]["max"] + " +";
            }
            aCommands.push(["_trackEvent", sSearchGroup, sCluster, sSearchString]);
        }
        this.trackSplitter(aCommands);
    }
});

