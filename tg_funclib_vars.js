tgNamespace.tgContainer.registerInnerFuncLib("ga", {


    "trackVar": function (sVarKey, sVarValue, iScope)
    {
        var aCommands = [];
        for (var i = 0; i < this.oLocalConfData.id.length; i++)
        {
            if (iScope === 1)
            {
                this.oVarValues[this.oLocalConfData.id[i]].uservars[sVarKey] = sVarValue;
                var oVarSlots = this.readConfig("aUserVarSlots", this.oLocalConfData.id[i]);
                var oVarValues = this.oVarValues[this.oLocalConfData.id[i]].uservars;
            }
            else
            {
                if (iScope === 2)
                {
                    this.oVarValues[this.oLocalConfData.id[i]].sessionvars[sVarKey] = sVarValue;
                    var oVarSlots = this.readConfig("aSessionVarSlots", this.oLocalConfData.id[i]);
                    var oVarValues = this.oVarValues[this.oLocalConfData.id[i]].sessionvars;
                }
                else
                {
                    if (iScope === 3)
                    {
                        this.oVarValues[this.oLocalConfData.id[i]].pagevars[sVarKey] = sVarValue;
                        var oVarSlots = this.readConfig("aPageVarSlots", this.oLocalConfData.id[i]);
                        var oVarValues = this.oVarValues[this.oLocalConfData.id[i]].pagevars;
                    }
                }
            }
            var iVarIndex = oVarSlots[sVarKey];
            var sMergedKey = false;
            var sMergedValues = false;
            for (var sKey in oVarSlots)
            {
                if (oVarSlots[sKey] == iVarIndex)
                {
                    if (typeof oVarValues[sKey] == "undefined")
                    {
                        oVarValues[sKey] = "+na+";
                    }
                    if (sMergedKey === false)
                    {
                        sMergedKey = sKey;
                        sMergedValues = oVarValues[sKey];
                    }
                    else
                    {
                        sMergedKey += " ; " + sKey;
                        sMergedValues += " ; " + oVarValues[sKey];
                    }
                }
            }
            if (sMergedKey !== false)
            {
                aCommands.push(["_setCustomVar", iVarIndex, sMergedKey, sMergedValues, iScope]);
            }
        }
        if (aCommands.length > 0)
        {
            this.trackSplitter(aCommands);
        }
    },
    "trackUserVar": function (oArg)
    {

        this.trackVar(oArg.key, oArg.value, 1);
    },
    "trackPageVar": function (oArg)
    {
        this.trackVar(oArg.key, oArg.value, 3);
    },
    "trackSessionVar": function (oArg)
    {
        this.trackVar(oArg.key, oArg.value, 2);
    },
    "saveVarMerging": function ()
    {
        for (var i = 0; i < this.oLocalConfData.id.length; i++)
        {
            var sCookieVal = JSON.stringify(this.oVarValues[this.oLocalConfData.id[i]]);
            for (var i = 0; i < this.oLocalConfData.id.length; i++)
            {
                this.oWrapper.setCookie(this.readConfig("sVarMergeCookieName", this.oLocalConfData.id[i]) + "_" + i, encodeURIComponent(sCookieVal), 730);
            }
        }
    }
});

